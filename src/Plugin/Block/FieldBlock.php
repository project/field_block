<?php

namespace Drupal\field_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\field\FieldStorageConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block to place a field within.
 *
 * @Block(
 *   id = "field_block_block",
 *   admin_label = @Translation("Field Block"),
 *   category = @Translation("Fields")
 * )
 */
class FieldBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * Constructs a new FieldBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_plugin_manager
   *   The field type plugin manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RouteMatchInterface $route_match,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    FieldTypePluginManagerInterface $field_type_plugin_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->fieldTypePluginManager = $field_type_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.field.field_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'entity_type_id' => 'node',
      'field_name' => '',
      'view_mode' => 'full',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $form['entity_type_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#options' => $this->getContentEntityTypeOptions(),
      '#default_value' => $this->configuration['entity_type_id'],
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'fieldNamesCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-field-name',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Updating field name list...'),
        ],
      ],
    ];

    $form['field_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Field'),
      '#options' => $this->getFieldOptions($this->configuration['entity_type_id']),
      '#default_value' => $this->configuration['field_name'],
      '#prefix' => '<div id="ajax-field-name">',
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['entity_type_id'] = $form_state->getValue('entity_type_id');
    $this->configuration['field_name'] = $form_state->getValue('field_name');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $entity_type_id = $this->configuration['entity_type_id'];
    $field_name = $this->configuration['field_name'];
    $view_mode = $this->configuration['view_mode'];
    /** @var \Drupal\Core\Entity\ContentEntityInterface|null */
    $entity = $this->routeMatch->getParameter($entity_type_id);
    $display_field = (
      $entity->hasField($field_name) &&
      !$entity->{$field_name}->isEmpty()
    );

    if ($display_field) {
      return [
        'content' => $entity->{$field_name}->view($view_mode),
      ];
    }

    return NULL;
  }

  /**
   * Gets the list of content entity types for select lists.
   *
   * @return array
   *   The list of content entity types in the format of ['id' => 'Label'].
   */
  protected function getContentEntityTypeOptions() {
    $entity_types = [];

    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $entity_types[$definition->id()] = $definition->getLabel();
      }
    }

    return $entity_types;
  }

  /**
   * Gets the list of field names for select lists.
   *
   * @param string $entity_type_id
   *   The entity type to look up the fields for.
   *
   * @return array
   *   The list of fields in the format of ['machine_name' => 'Label'].
   */
  protected function getFieldOptions($entity_type_id) {
    $field_definitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_id);
    $field_types = $this->fieldTypePluginManager->getDefinitions();
    $fields = [];

    foreach ($field_definitions as $definition) {
      if ($definition instanceof FieldStorageConfigInterface) {
        $field_name = $definition->getName();

        $fields[$field_name] = $this->t('@type: @field', [
          '@type' => $field_types[$definition->getType()]['label'],
          '@field' => $field_name,
        ]);
      }
    }

    return $fields;
  }

  /**
   * Callback function for the content type form change.
   */
  public function fieldNamesCallback(array &$form, FormStateInterface $form_state) {
    if ($form_settings = $form_state->getValue('settings')) {
      $options = $this->getFieldOptions($form_settings['entity_type_id']);
      $form['settings']['field_name']['#options'] = $options;
    }

    return $form['settings']['field_name'];
  }

}
